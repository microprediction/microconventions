import re
import sys

GITLAB = "https://gitlab.com/microprediction/microconventions/-/blob/master/microconventions/conventions.py"
GITHUB = "https://github.com/microprediction/microconventions/blob/master/microconventions/conventions.py"


def convert_line(line):
    x = re.search('\/\/github\.com\/([\w.\/]*)\/blob\/([\w\/.]*)', line)
    if x is None:
        return line
    else:
        after = '//gitlab.com/' + x.groups()[0] + '/-/blob/' + x.groups()[1]
        before = '//github.com/'+ x.groups()[0] + '/blob/' + x.groups()[1]
        print(before +' -> '+after)
        new_line = line.replace(before,after)
        return new_line

assert convert_line('blah '+GITHUB+' BLAH') == 'blah '+GITLAB+' BLAH'


if __name__=='__main__':
    file_name = sys.argv[1]
    with open(file_name, 'r') as infile:
        data = infile.readlines()
    with open(file_name,'w') as outfile:
        outfile.writelines([convert_line(line) for line in data])


